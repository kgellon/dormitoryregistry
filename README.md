WinForms:

A feladat egy kollégiumi foglalkozásokat nyilvántartó program elkészítése lesz.
Az elkészítéshez WinFormst vagy WPF –et használj (WinForms a preferált), az adatbázis motor pedig
Firebird vagy MsSQL legyen. Ha ismersz, akkor használj valamilyen ORM rendszert, pl. DevExpress XPO
vagy Entity Framework.
Készíts egy olyan nyilvántartó alkalmazást, amelybe különböző jogkörökkel rendelkező felhasználók
léphetnek be és foglalkozásokat kezelhetnek, illetve kérdezhetnek le.

Felhasználók:
Nevet, jogkört, állapotot, felhasználónevet és jelszót kell tárolni róluk.
- A név és jelszó tetszőleges, legfeljebb 50 karakteres szöveges érték lehet.
- Az állapot egy flag, ami az aktív / törölt státuszt jelöli.
- A felhasználónév csak kis/nagybetűket és számjegyeket tartalmazhat, valamint legfeljebb 20
karakter hosszú lehet.
- A jogkör háromféle lehet: vezető, tanár, diák.

Foglalkozások:
Megnevezést, rövid leírást, oktatót, időtartamot és kezdési időt kell róluk tárolni.
- A megnevezés egy legfeljebb 50 karakteres szöveg.
- A leírás 1500 karakteres, többsoros tetszőleges szöveg lehet.
- Az időtartam egy pozitív egész szám, amit percben kell érteni és a foglalkozás hosszát adja meg.
- A kezdési időhöz a hét egy napját és egy időpontot kell majd tárolni (pl. szerda 18:00), ahol a napot
választani lehessen, de az időpont tetszőlegesen szerkeszthető egy 12:00-20:00 közötti időtartamban.
A törzsadatokon kívül szükség lesz még egy detail táblára, ahol a foglalkozásokhoz diákok
rendelhetőek (a felhasználók táblából).

Működés:
Legyen egy beépített „Admin” felhasználó, aki vezető jogkörrel rendelkezik: az alkalmazás első
indításakor automatikusan hozzuk létre az adatbázisban.

A program induláskor kérjen be egy felhasználónév + jelszó párost, majd annak létezése esetén
(inaktív felhasználókat ne vegye figyelembe) az ahhoz tartozó jogkörrel induljon el.
A programban meg kell jeleníteni egy felületet, ahonnan elérhetőek a felhasználók és a foglalkozások
is, ez lehet pl. WinForms esetén egy MDI keretablak menüvel ellátva.
A felhasználók csak vezető vagy tanár szintű jogosultsággal jeleníthetőek meg.

Lehetőséget kell biztosítani a felhasználói lista megtekintésére, új felhasználó létrehozására,
meglévők módosítására és inaktiválására az alábbi megszorításokkal:
- A jelszót csak maszkolva jelenítsük meg.
- A felületen a tanár jogkörrel rendelkezők nem láthatják az inaktív felhasználókat.
- A kiemelt Admin felhasználó azonosítója, jogköre és jelszava nem módosítható, valamint nem
inaktiválható (vezető és saját maga által sem).
- Tanár jogkörrel rendelkező felhasználó csak diákot vehet fel/módosíthat/inaktiválhat, kivéve a saját
felhasználója, aminek módosíthatja a jelszavát. Vezető jogkörrel rendelkező vehet fel tanárt, diákot és
másik vezetőt is, valamint ezeket szabadon módosíthatja/inaktiválhatja is (kivéve az Admin –t, lsd.
előző pontban).
- Azonos felhasználóneveket ne lehessen berögzíteni.
A foglalkozások minden jogkörrel megtekinthetőek. A megjelenítés történhet pl. úgy, hogy a felület
bal oldalán egy listában láthatóak az alap információk (megnevezés, kezdési idő és oktató), jobb
oldalon pedig a kiválasztott foglalkozás részletei: felül a leírás és időtartam, alul pedig a tanulók
listája. Az alábbi megszorítások érvényesek:
- Diákok csak megtekinthetik a foglalkozásokat (semmilyen műveletet nem végezhetnek velük), de a
tanulók listáját is csak azoknál láthatják, amelyeken ők is részt vesznek.
- Tanár jogkörrel csak saját magának rögzíthet a felhasználó foglalkozást és csak a sajátjait
módosíthatja / törölheti.
- Tanár / vezető jogkörrel egy foglalkozáshoz fel lehet venni diákokat, illetve a résztvevők listája
később módosítható (törlés és felhasználó csere). Egy diák csak egyszer szerepeljen egy adott
foglalkozásnál.
A foglalkozások felületen a jobb felső részen redundánsan megjeleníthetőek a listában is szereplő
adatok a szerkesztés / új felvitel megkönnyítésére.
Néhány extra ellenőrzés beépíthető, de ezek nem kötelezőek:
- Mivel diákoknak csak a foglalkozások láthatóak, így induláskor egyből meg is lehet jeleníteni nekik.
- A foglalkozások nem nyúlhatnak 21:00 utánra, amit a kezdési idő + időtartam alapján kell ellenőrizni.
- Egy foglalkozáshoz legfeljebb 30 diákot lehessen berögzíteni.
- Ha egy diákot felveszünk egy foglalkozáshoz, de már van ezzel ütköző elfoglaltsága, akkor erről
jelenjen meg egy figyelmeztetés.
- Felhasználók jelszavát kódolva tároljuk.
- A felhasználónév kezelése (belépés, ütközés ellenőrzés) ne legyen érzékeny a kis/nagybetűkre.