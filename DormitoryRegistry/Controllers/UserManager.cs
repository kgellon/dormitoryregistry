﻿using DormitoryRegistry.DataAccessLayer.Enumerations;
using DormitoryRegistry.DataAccessLayer.Models;
using DormitoryRegistry.Extensions;
using DormitoryRegistry.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DormitoryRegistry.Controllers
{
    public sealed class UserManager
    {
        private LoginForm _LoginForm;
        private DatabaseManager _databaseManager;

        private User _activeUser;
        public User ActiveUser
        {
            get { return _activeUser; }
            private set { _activeUser = value; }
        }

        public Roles? ActiveUserRole
        {
            get { return ActiveUser != null && Enum.TryParse(ActiveUser.Role.Name, out Roles role) ? role: (Roles?)null; }
        }

        public bool HasActiveUser
        {
            get { return _activeUser != null; }
        }

        public UserManager(DatabaseManager databaseManager)
        {
            _databaseManager = databaseManager;

            _LoginForm = new LoginForm();
            _LoginForm.DataCheckRequired += LoginForm_DataCheckRequired;
        }

        public void ShowLogin()
        {
            if (_LoginForm.IsShown == false)
            {
                _LoginForm.Clear();
                _LoginForm.ShowDialog();
            }
        }

        private async void LoginForm_DataCheckRequired(object sender, EventArgs e)
        {
            _LoginForm.ShowInformationMessage("Adatok ellenőrzése...", false);
            ActiveUser = await GetUser(_LoginForm.Username, _LoginForm.Password.ToSHA256String());
            if(ActiveUser == null)
            {
                _LoginForm.ShowErrorMessage("Hibás felhasználónév vagy jelszó...", true);
            }
            else
            {
                _LoginForm.Close();
            }
        }

        private async Task<User> GetUser(string username, string password)
        {
            User result = null;

            if (_databaseManager.Initialized)
            {
                result = await Task.Run(() =>
                {
                    return _databaseManager.UserService.GetUser(username, password);
                });
            }
            return result;
        }

        private async Task<Role> GetUserRole(Guid roleId)
        {
            Role result = null;

            if (_databaseManager.Initialized)
            {
                result = await Task.Run(() =>
                {
                    return _databaseManager.RoleService.GetRoleById(roleId);
                });
            }
            return result;
        }
    }
}
