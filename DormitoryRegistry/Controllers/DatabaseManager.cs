﻿using DormitoryRegistry.DataAccessLayer;
using DormitoryRegistry.DataAccessLayer.Interfaces;
using DormitoryRegistry.DataAccessLayer.Models;
using DormitoryRegistry.DataAccessLayer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Reflection;
using DormitoryRegistry.DataAccessLayer.Enumerations;

namespace DormitoryRegistry.Controllers
{
    public class DatabaseManager
    {
        private MainController _mainController;

        public event EventHandler<int> ProgressChanged;

        public event EventHandler<Exception> InternalErrorOccured;

        public event EventHandler InitializationCompleted;


        public IRightService RightService { get; }
        public IRoleService RoleService { get; }

        public IUserService UserService { get; }

        public DatabaseManager(MainController mainController)
        {
            _mainController = mainController;
            RightService = new RightService();
            RoleService = new RoleService();
            UserService = new UserService();
        }

        public bool Initialized { get; private set; }

        public async void Initialize()
        {
            await Task.Run(() => 
            {
                Thread.Sleep(100);
                OnProgressChanged(10);
                RightService.Initialize();
                OnProgressChanged(40);
                RoleService.Initialize();
                OnProgressChanged(70);
                UserService.Initialize();
                OnProgressChanged(100);
                Thread.Sleep(500);
                Initialized = true;
            }).ContinueWith((parentTask) => 
            {
                if(parentTask.Exception != null)
                {
                    OnInternalErrorOccured(parentTask.Exception);
                }
                else
                {
                    OnInitializationCompleted();
                }
            });
        }

        protected void OnProgressChanged(int progress)
        {
            ProgressChanged?.Invoke(this, progress);
        }

        protected void OnInternalErrorOccured(Exception exception)
        {
            if (exception != null)
            {
                InternalErrorOccured?.Invoke(this, exception);
            }
        }

        protected void OnInitializationCompleted()
        {
            InitializationCompleted?.Invoke(this, EventArgs.Empty);
        }
    }
}
