﻿using DormitoryRegistry.DataAccessLayer.Enumerations;
using DormitoryRegistry.DataAccessLayer.Models;
using DormitoryRegistry.Extensions;
using DormitoryRegistry.View;
using DormitoryRegistry.View.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DormitoryRegistry.Controllers
{
    public class MainController
    {
        private static readonly object _syncRoot = new object();
        private static volatile MainController _instance;
        public static MainController Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncRoot) { _instance = _instance ?? new MainController(); }
                }
                return _instance;
            }
        }

        private MainController()
        { }

        private DatabaseManager _databaseManager;
        private NotificationController _notificationController;
        private UserManager _userManager;

        private MainForm _mainView;

        public bool Initialized { get; private set; }
        public bool Initializing { get; private set; }

        public BindingList<User> DisplayedUsers;

        public event EventHandler SaveCompleted;
        public event EventHandler<string> SaveFailed;

        public bool Inititalize()
        {
            if (Initialized == false && Initializing == false)
            {
                Initializing = true;

                DisplayedUsers = new BindingList<User>();

                _notificationController = new NotificationController(this);
                
                _databaseManager = new DatabaseManager(this);
                _databaseManager.ProgressChanged += DatabaseController_ProgressChanged;
                _databaseManager.InternalErrorOccured += DatabaseController_InternalErrorOccured;
                _databaseManager.InitializationCompleted += DatabaseController_InitializationCompleted;

                _databaseManager.Initialize();
                _notificationController.ShowProgressNotification("Üdvözlöm", "Betöltés folyamatban...", 0);
                Initialized = _databaseManager.Initialized;
                Initializing = false;
            }
            return Initialized;
        }

        public MainForm InitializeMainForm()
        {
            _mainView = new MainForm(this);
            _mainView.Shown += MainView_Shown;
            _mainView.SelectionChanged += MainView_SelectionChanged;

            _mainView.Content.NewSaveRequired += Content_NewSaveRequired;
            return _mainView;
        }

        private void Content_NewSaveRequired(object sender, EventModels.NewUserEventArgs e)
        {
            if(e != null)
            {
                if(_databaseManager.UserService.CheckUserName(e.LoginName))
                {
                    SaveFailed?.Invoke(this, "A felhasználónév már létezik.");
                }
                else
                {
                    Roles roleType = Roles.Leader;
                    roleType = e.ContentType == View.Enumerations.ContentType.Leaders ? Roles.Leader : roleType;
                    roleType = e.ContentType == View.Enumerations.ContentType.Students ? Roles.Student : roleType;
                    roleType = e.ContentType == View.Enumerations.ContentType.Teachers ? Roles.Teacher : roleType;

                    Role role = _databaseManager.RoleService.GetRoleByName(roleType);

                    if(role != null)
                    {
                        User user = new User()
                        {
                            UserId = Guid.NewGuid(),
                            FirstName = e.FirstName,
                            LastName = e.LastName,
                            LoginName = e.LoginName,
                            Password = e.Password,
                            RoleId = role.RoleId
                        };
                        _databaseManager.UserService.Insert(user);

                        SaveCompleted?.Invoke(this, EventArgs.Empty);
                        UpdateDataLists();
                    }
                }
            }
        }

        private void MainView_SelectionChanged(object sender, EventArgs e)
        {
            UpdateDataLists();
            _mainView.Content.SelectPanelByType(_mainView.MenuStrip.Selected);
        }

        private void UpdateDataLists()
        {
            if (_userManager.ActiveUserRole.HasValue)
            {
                DisplayedUsers.Clear();
                _databaseManager.UserService.GetAll().ForEach(user =>
                {
                    if (Enum.TryParse(user.Role.Name, out Roles role))
                    {
                        if ((_mainView.MenuStrip.Selected == ContentType.Leaders && role == Roles.Leader) ||
                           (_mainView.MenuStrip.Selected == ContentType.Students && role == Roles.Student) ||
                           (_mainView.MenuStrip.Selected == ContentType.Teachers && role == Roles.Teacher))
                        {
                            DisplayedUsers.Add(user);
                        }
                    }

                });
            }
        }

        private void InititalizeUserManager()
        {
            _userManager = new UserManager(_databaseManager);
        }

        private void MainView_Shown(object sender, EventArgs e)
        {
            InititalizeUserManager();
            _mainView.Opacity = 0.8f;
            _userManager.ShowLogin();

            if(_userManager.HasActiveUser)
            {
                ResetMainView();
            }
            else
            {
                Close();
            }
        }

        private void ResetMainView()
        {
            _mainView.Opacity = 1f;

            switch(_userManager.ActiveUserRole)
            {
                case Roles.Leader:
                    _mainView.MenuStrip.LeadersVisible = true;
                    _mainView.MenuStrip.TeachersVisible = true;
                    _mainView.Content.UserAddEnabled = true;
                    _mainView.Content.ShowDeletedState = true;
                    break;
                case Roles.Student:
                    _mainView.MenuStrip.LeadersVisible = false;
                    _mainView.MenuStrip.TeachersVisible = false;
                    _mainView.Content.UserAddEnabled = false;
                    _mainView.Content.ShowDeletedState = false;
                    break;
                case Roles.Teacher:
                    _mainView.MenuStrip.LeadersVisible = false;
                    _mainView.MenuStrip.TeachersVisible = true;
                    _mainView.Content.UserAddEnabled = true;
                    _mainView.Content.ShowDeletedState = false;
                    break;
            }

            _mainView.UserDisplayName = string.Format("{0} {1}", _userManager.ActiveUser.LastName, _userManager.ActiveUser.FirstName);
        }

        public void Close()
        {
            _mainView.Close();
        }

        private void DatabaseController_InitializationCompleted(object sender, EventArgs e)
        {
            _notificationController.CloseNotification();
        }

        private void DatabaseController_InternalErrorOccured(object sender, Exception e)
        {
            _notificationController.ShowErrorNotification("Hiba történt...", e.GetBaseException().Message, true);
        }

        private void DatabaseController_ProgressChanged(object sender, int e)
        {
            _notificationController.UpdateProgress(e);
        }
    }
}
