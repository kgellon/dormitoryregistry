﻿using DormitoryRegistry.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DormitoryRegistry.Controllers
{
    public sealed class NotificationController
    {
        private NotificationForm _notificationDisplay;
        private MainController _mainController;

        public event FormClosedEventHandler NotificationClosed
        {
            add { _notificationDisplay.FormClosed += value; }
            remove { _notificationDisplay.FormClosed -= value; }
        }

        public bool IsShown
        {
            get { return _notificationDisplay.IsShown; }
        }

        public NotificationController(MainController mainController)
        {
            _mainController = mainController;

            _notificationDisplay = new NotificationForm();
        }

        private void NotificationDisplay_FormClosed(object sender, FormClosedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void NotificationDisplay_Shown(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public void ShowInfoNotification(string title, string message)
        {

            if (_notificationDisplay.InvokeRequired)
            {
                _notificationDisplay.BeginInvoke((MethodInvoker)(() => { ShowInfoNotification(title, message); }));
            }
            else
            {
                _notificationDisplay.NotificationType = NotificationForm.NotificationTypes.Information;
                _notificationDisplay.Title = title;
                _notificationDisplay.Message = message;

                if (_notificationDisplay.IsShown == false)
                {
                    _notificationDisplay.ShowDialog();
                }
            }
        }

        public void ShowProgressNotification(string title, string message, int initialProgress = 0)
        {
            if (_notificationDisplay.InvokeRequired)
            {
                _notificationDisplay.BeginInvoke((MethodInvoker)(() => { ShowProgressNotification(title, message, initialProgress); }));
            }
            else
            {
                _notificationDisplay.NotificationType = NotificationForm.NotificationTypes.Progress;
                _notificationDisplay.Title = title;
                _notificationDisplay.Message = message;

                if (_notificationDisplay.IsShown == false)
                {
                    _notificationDisplay.ShowDialog();
                }
            }
        }

        public void ShowErrorNotification(string title, string message, bool showCloseButton = true)
        {
            if (_notificationDisplay.InvokeRequired)
            {
                _notificationDisplay.BeginInvoke((MethodInvoker)(() => { ShowErrorNotification(title, message, showCloseButton); }));
            }
            else
            {
                _notificationDisplay.NotificationType = NotificationForm.NotificationTypes.Error;
                _notificationDisplay.Title = title;
                _notificationDisplay.Message = message;
                _notificationDisplay.CloseVisible = showCloseButton;

                if (_notificationDisplay.IsShown == false)
                {
                    _notificationDisplay.ShowDialog();
                }
            }
        }

        public void UpdateProgress(int progress)
        {
            if (_notificationDisplay.InvokeRequired)
            {
                _notificationDisplay.BeginInvoke((MethodInvoker)(() => { UpdateProgress(progress); }));
            }
            else
            {
                if(_notificationDisplay.IsShown && _notificationDisplay.NotificationType == NotificationForm.NotificationTypes.Progress)
                {
                    _notificationDisplay.Progress = progress;
                }
            }
        }

        public void CloseNotification()
        {
            if(_notificationDisplay.InvokeRequired)
            {
                _notificationDisplay.BeginInvoke((MethodInvoker)(() => { _notificationDisplay.Close(); }));
            }
            else
            {
                _notificationDisplay.Close();
            }
        }
    }
}
