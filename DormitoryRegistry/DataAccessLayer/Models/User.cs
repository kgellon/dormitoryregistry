﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DormitoryRegistry.DataAccessLayer.Models
{
    public class User
    {
        public Guid UserId { get; set; }

        [Index(IsUnique = true), StringLength(50)]
        public string LoginName { get; set; }
        [StringLength(100)]
        public string Password { get; set; }

        [StringLength(100)]
        public string FirstName { get; set; }
        [StringLength(100)]
        public string LastName { get; set; }

        [DefaultValue("false")]
        public bool Deleted { get; set; }

        public Guid RoleId { get; set; }

        public virtual Role Role { get; set; }
    }
}
