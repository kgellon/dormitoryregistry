﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DormitoryRegistry.DataAccessLayer.Models
{
    public class Role
    {
        public Guid RoleId { get; set; }

        [Index(IsUnique = true), StringLength(50)]
        public string Name { get; set; }

        [Index(IsUnique = true)]
        public byte TypeCode { get; set; }

        public DateTime ValidFrom { get; set; }

        [DefaultValue(null)]
        public DateTime? ValidTo { get; set; }
    }
}
