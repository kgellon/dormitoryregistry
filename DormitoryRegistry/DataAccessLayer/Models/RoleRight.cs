﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DormitoryRegistry.DataAccessLayer.Models
{
    public class RoleRight
    {
        public Guid RoleRightId { get; set; }

        public Guid RoleId { get; set; }

        public Guid RightId { get; set; }

        public virtual Role Role { get; set; }

        public virtual Right Right { get; set; }

    }
}
