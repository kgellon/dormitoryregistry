﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DormitoryRegistry.DataAccessLayer.Models
{
    public class Right
    {
        public Guid RightId { get; set; }

        [Index(IsUnique = true), StringLength(50)]
        public string Name { get; set; }

        public string Type { get; set; }
    }
}
