﻿using DormitoryRegistry.DataAccessLayer.Enumerations;
using DormitoryRegistry.DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DormitoryRegistry.DataAccessLayer.Interfaces
{
    public interface IRoleService
    {
        void Initialize();

        Role GetRoleByName(Roles role);

        Role GetRoleById(Guid roleId);

        List<Role> GetAll();

        List<Right> GetRights(Guid roleId);
    }
}
