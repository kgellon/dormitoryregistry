﻿using DormitoryRegistry.DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DormitoryRegistry.DataAccessLayer.Interfaces
{
    public interface IRightService
    {
        void Initialize();

        List<Right> GetAll();

        List<Right> GetRights(Guid userId);
    }
}
