﻿using DormitoryRegistry.DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DormitoryRegistry.DataAccessLayer.Interfaces
{
    public interface IUserService
    {
        void Initialize();

        bool CheckUserName(string username);

        User GetUser(Guid userId);

        User GetUser(string username, string shaPassword);

        List<User> GetAll();

        bool Insert(User user);

        bool Delete(User user);

        bool Delete(Guid userId);
    }
}
