﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DormitoryRegistry.DataAccessLayer.Enumerations
{
    public enum Rights
    {
        //User management rights
        [Description("Hozzáadhat vezetőt")]
        AddLeader,
        [Description("Hozzáadhat tanárt")]
        AddTeacher,
        [Description("Hozzáadhat diákot")]
        AddStudent,


        [Description("Láthat inaktív felhasználókat")]
        SeeInactiveUsers,

        [Description("Aktiválhat felhasználót")]
        ActivateUser,
        [Description("Deaktiválhat felhasználót")]
        DeactivateUser,

        //Occupation rights
    }
}
