﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DormitoryRegistry.DataAccessLayer.Enumerations
{
    public enum Roles
    {
        Leader = 0x00,
        Teacher = 0x10,
        Student = 0x20
    }
}
