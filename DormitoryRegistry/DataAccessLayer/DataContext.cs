﻿using DormitoryRegistry.DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DormitoryRegistry.DataAccessLayer
{
    public class DataContext: DbContext
    {
        public DbSet<User> Users { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Right> Rights { get; set; }

        public DbSet<RoleRight> RoleRights { get; set; }
    }
}
