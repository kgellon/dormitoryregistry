﻿using DormitoryRegistry.DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DormitoryRegistry.DataAccessLayer.Models;
using DormitoryRegistry.DataAccessLayer.Enumerations;

namespace DormitoryRegistry.DataAccessLayer.Services
{
    public sealed class RoleService : IRoleService
    {
        public void Initialize()
        {
            try
            {
                List<Role> initialList = GetAll();

                foreach (Roles role in Enum.GetValues(typeof(Roles)))
                {
                    if (initialList.Any(item => item.Name == role.ToString()) == false)
                    {
                        var newRole = new Role()
                        {
                            RoleId = Guid.NewGuid(),
                            Name = role.ToString(),
                            ValidFrom = DateTime.UtcNow,
                            ValidTo = null,
                            TypeCode = (byte)role
                        };

                        Insert(newRole);
                    }
                }
            }
            catch (Exception e)
            {
                //TODO --> logging 
                throw e;
            }
        }

        public bool Insert(Role role)
        {
            bool result = true;

            try
            {
                using (DataContext context = new DataContext())
                {
                    context.Roles.Add(role);
                    context.SaveChanges();
                    result = true;
                }
            }
            catch (Exception e)
            {
                //TODO --> logging 
                throw e;
            }

            return result;
        }

        public Role GetRoleByName(Roles role)
        {
            Role result = null;

            try
            {
                using (DataContext context = new DataContext())
                {
                    result = context.Roles.FirstOrDefault(item => item.Name == role.ToString());
                }
            }
            catch (Exception e)
            {
                //TODO --> logging 
                throw e;
            }

            return result;
        }

        public List<Role> GetAll()
        {
            List<Role> result = null;

            try
            {
                using (DataContext context = new DataContext())
                {
                    result = context.Roles.ToList();
                }
            }
            catch (Exception e)
            {
                //TODO --> logging 
                throw e;
            }

            return result;
        }

        public List<Right> GetRights(Guid roleId)
        {
            throw new NotImplementedException();
        }

        public Role GetRoleById(Guid roleId)
        {
            Role result = null;

            try
            {
                using (DataContext context = new DataContext())
                {
                    result = context.Roles.FirstOrDefault(item => item.RoleId == roleId);
                }
            }
            catch (Exception e)
            {
                //TODO --> logging 
                throw e;
            }

            return result;
        }
    }
}
