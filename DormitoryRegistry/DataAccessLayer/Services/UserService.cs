﻿using DormitoryRegistry.DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DormitoryRegistry.DataAccessLayer.Models;
using DormitoryRegistry.Extensions;

namespace DormitoryRegistry.DataAccessLayer.Services
{
    public class UserService : IUserService
    {
        public const string DefaultAdminUser = "admin";
        private const string DefaultAdminPassword = "8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918";

        public void Initialize()
        {
            try
            {
                List<User> initialUsers = GetAll();
                Role leaderRole = new RoleService().GetRoleByName(Enumerations.Roles.Leader);

                if(initialUsers != null && initialUsers.Any(user => user.LoginName == DefaultAdminUser) == false)
                {

                    var adminUser = new User()
                    {
                        UserId = Guid.NewGuid(),
                        FirstName = "Default",
                        LastName = "Admin",
                        LoginName = DefaultAdminUser,
                        Password = DefaultAdminPassword,
                        RoleId = leaderRole.RoleId
                    };

                    Insert(adminUser);
                }
            }
            catch (Exception e)
            {
                //TODO --> logging 
                throw e;
            }
        }

        public bool CheckUserName(string username)
        {
            bool result = false;
            try
            {
                using (DataContext context = new DataContext())
                {
                    result = context.Users.FirstOrDefault(user => user.LoginName == username) != null;
                }
            }
            catch (Exception e)
            {
                //TODO --> logging 
                throw e;
            }

            return result;
        }

        public bool Delete(User user)
        {
            bool result = false;
            try
            {
                using (DataContext context = new DataContext())
                {
                    if (context.Users.Contains(user))
                    {
                        context.Users.Remove(user);
                        context.SaveChanges();
                    }
                    result = true;
                }
            }
            catch (Exception e)
            {
                //TODO --> logging 
                throw e;
            }

            return result;
        }

        public bool Delete(Guid userId)
        {
            bool result = false;
            try
            {
                using (DataContext context = new DataContext())
                {
                    var user = context.Users.FirstOrDefault(item => item.UserId == userId);
                    if(user != null)
                    {
                        context.Users.Remove(user);
                        context.SaveChanges();
                    }
                    result = true;
                }
            }
            catch (Exception e)
            {
                //TODO --> logging 
                throw e;
            }

            return result;
        }

        public List<User> GetAll()
        {
            List<User> result = null;

            try
            {
                using (DataContext context = new DataContext())
                {
                    result = context.Users.ToList();
                    Role temp;
                    result.ForEach(user => temp = user.Role);
                }
            }
            catch (Exception e)
            {
                //TODO --> logging 
                throw e;
            }

            return result;
        }

        public User GetUser(Guid userId)
        {
            throw new NotImplementedException();
        }

        public User GetUser(string username, string shaPassword)
        {
            User result = null;

            try
            {
                using (DataContext context = new DataContext())
                {
                    result = context.Users.FirstOrDefault(user => user.LoginName == username && user.Password == shaPassword);
                    
                    if (result != null)
                    {
                        Role getRole = result.Role;
                    }
                }
            }
            catch (Exception e)
            {
                //TODO --> logging 
                throw e;
            }

            return result;
        }

        public bool Insert(User user)
        {
            bool result = false;
            try
            {
                using (DataContext context = new DataContext())
                {
                    context.Users.Add(user);
                    context.SaveChanges();
                    result = true;
                }
            }
            catch (Exception e)
            {
                //TODO --> logging 
                throw e;
            }

            return result;
        }
    }
}
