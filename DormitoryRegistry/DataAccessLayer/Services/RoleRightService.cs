﻿using DormitoryRegistry.DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using DormitoryRegistry.DataAccessLayer.Models;

namespace DormitoryRegistry.DataAccessLayer.Services
{
    public sealed class RoleRightService
    {
        public void Initialize()
        {
            try
            {
                IRoleService roleSerive = new RoleService();
                IRightService rightService = new RightService();

                List<RoleRight> initialList = GetAll();
                
            }
            catch (Exception e)
            {
                //TODO --> logging 
            }
        }

        public bool Insert(Role role)
        {
            bool result = true;

            try
            {
                using (DataContext context = new DataContext())
                {
                    context.Roles.Add(role);
                    context.SaveChanges();
                    result = true;
                }
            }
            catch
            {
                //TODO --> logging 
            }

            return result;
        }

        public List<RoleRight> GetAll()
        {
            List<RoleRight> result = null;

            try
            {
                using (DataContext context = new DataContext())
                {
                    result = context.RoleRights.ToList();
                }
            }
            catch
            {
                //TODO --> logging 
            }

            return result;
        }

        public List<Right> GetRights(Guid roleId)
        {
            throw new NotImplementedException();
        }

       
    }
}
