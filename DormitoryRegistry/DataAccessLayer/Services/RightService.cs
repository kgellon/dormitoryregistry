﻿using DormitoryRegistry.DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DormitoryRegistry.DataAccessLayer.Models;
using DormitoryRegistry.DataAccessLayer.Enumerations;

namespace DormitoryRegistry.DataAccessLayer.Services
{
    public sealed class RightService : IRightService
    {
        public void Initialize()
        {
            try
            {
                List<Right> initialList = GetAll();
                List<Right> newRights = new List<Right>();

                foreach (string rightName in Enum.GetNames(typeof(Rights)))
                {
                    if (initialList.Any(item => item.Name == rightName) == false)
                    {
                        var newItem = new Right()
                        {
                            RightId = Guid.NewGuid(),
                            Name = rightName,
                            Type = string.Empty
                        };

                        newRights.Add(newItem);
                    }
                }

                Insert(newRights);
            }
            catch (Exception e)
            {
                //TODO --> logging 
                throw e;
            }
        }

        private bool Insert(Right right)
        {
            bool result = false;
            try
            {
                using (DataContext context = new DataContext())
                {
                    context.Rights.Add(right);
                    context.SaveChanges();
                    result = true;
                }
            }
            catch (Exception e)
            {
                //TODO --> logging 
                throw e;
            }

            return result;
        }

        private bool Insert(List<Right> rights)
        {
            bool result = false;
            try
            {
                if (rights != null && rights.Count > 0)
                {
                    using (DataContext context = new DataContext())
                    {
                        context.Rights.AddRange(rights);
                        context.SaveChanges();
                        result = true;
                    }
                }
            }
            catch (Exception e)
            {
                //TODO --> logging 
                throw e;
            }

            return result;
        }

        private bool Delete(string rightname)
        {
            bool result = false;
            try
            {
                if (string.IsNullOrEmpty(rightname) == false)
                {
                    using (DataContext context = new DataContext())
                    {
                        Right right = context.Rights.FirstOrDefault(item => item.Name == rightname);

                        if(right != null)
                        {
                            context.RoleRights.RemoveRange(context.RoleRights.Where(item => item.RightId == right.RightId).ToList());
                            context.Rights.Remove(right);
                            context.SaveChanges();
                        }

                        result = true;
                    }
                }
            }
            catch (Exception e)
            {
                //TODO --> logging 
                throw e;
            }

            return result;
        }

        private bool Delete(Guid rightId)
        {
            throw new NotImplementedException();
        }

        public List<Right> GetAll()
        {
            List<Right> result = null;
            try
            {
                using (DataContext context = new DataContext())
                {
                    result = context.Rights.ToList();
                }
            }
            catch (Exception e)
            {
                //TODO --> logging 
                throw e;
            }

            return result;
        }

        public List<Right> GetRights(Guid userId)
        {
            List<Right> result = null;
            try
            {
                using (DataContext context = new DataContext())
                {
                    Guid roleId = context.Users.FirstOrDefault(user => user.UserId == userId).RoleId;
                }
            }
            catch(Exception e)
            {
                //TODO --> logging 
                throw e;
            }

            return result;
        }

        public List<Right> GetDefaultRights(Roles role)
        {
            List<Right> rights = null;
            try
            {
                rights = GetAll();
                foreach (Right right in GetAll())
                {
                    if (Enum.TryParse(right.Name, out Rights rightName))
                    {
                        switch (rightName)
                        {
                            case Rights.ActivateUser:
                            case Rights.DeactivateUser:
                            case Rights.SeeInactiveUsers:
                            case Rights.AddLeader:
                            case Rights.AddStudent:
                            case Rights.AddTeacher:
                                rights.Add(right);
                                break;
                            default: break;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                //TODO --> logging 
                throw e;
            }

            return rights;
        }
    }
}
