﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DormitoryRegistry.Extensions
{
    public static class DataExtensions
    {
        public static string ToSHA256String<T>(this T value)
        {
            string result = string.Empty;

            using (var hash = SHA256.Create())
            {
                byte[] databytes = hash.ComputeHash(Encoding.UTF8.GetBytes(value.ToString()));
                result = BitConverter.ToString(databytes).Replace("-", "").ToLowerInvariant();
            }

            return result;
        }

    }
}
