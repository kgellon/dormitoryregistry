﻿using DormitoryRegistry.Controllers;
using DormitoryRegistry.View;
using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DormitoryRegistry
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            
            if (MainController.Instance.Inititalize())
            {
                Application.Run(MainController.Instance.InitializeMainForm());
            }
        }
    }
}
