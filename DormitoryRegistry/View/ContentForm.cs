﻿using DormitoryRegistry.Controllers;
using DormitoryRegistry.View.ContentPanels;
using DormitoryRegistry.View.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DormitoryRegistry.EventModels;

namespace DormitoryRegistry.View
{
    public partial class ContentForm : Form
    {
        MainController _mainController;

        public event NewUserEventHandler NewSaveRequired
        {
            add { _usersPanel.NewSaveRequired += value; }
            remove { _usersPanel.NewSaveRequired -= value; }
        }

        public bool UserAddEnabled
        {
            get { return _usersPanel.AddEnabled; }
            set { _usersPanel.AddEnabled = value; }
        }

        public bool ShowDeletedState
        {
            get { return _usersPanel.UserGridView.Columns["Deleted"].Visible; }
            set { _usersPanel.UserGridView.Columns["Deleted"].Visible = value; }
        }

        private UsersPanel _usersPanel;

        public ContentForm(MainController mainController)
        {
            _mainController = mainController;
            _mainController.SaveCompleted += MainController_SaveCompleted;
            _mainController.SaveFailed += MainController_SaveFailed;
            InitializeComponent();
            _usersPanel = new UsersPanel();

            _usersPanel.UserGridView.DataSource = _mainController.DisplayedUsers;
            _usersPanel.Dock = DockStyle.Fill;
        }

        private void MainController_SaveFailed(object sender, string e)
        {
            _usersPanel.ShowErrorMessage(e);
        }

        private void MainController_SaveCompleted(object sender, EventArgs e)
        {
            _usersPanel.ClearFileds();
            _usersPanel.ClearMessage();
        }

        public void SelectPanelByType(ContentType type)
        {
            if (Controls.Contains(_usersPanel))
            {
                Controls.Remove(_usersPanel);
            }

            switch(type)
            {
                case ContentType.Leaders:
                    _usersPanel.Title = "Vezetők";
                    _usersPanel.ContentType = type;
                    Controls.Add(_usersPanel);
                    break;
                case ContentType.Students:
                    _usersPanel.Title = "Tanulók";
                    _usersPanel.ContentType = type;
                    Controls.Add(_usersPanel);
                    break;
                case ContentType.Teachers:
                    _usersPanel.Title = "Tanárok";
                    _usersPanel.ContentType = type;
                    Controls.Add(_usersPanel);
                    break;
            }
        }
    }
}
