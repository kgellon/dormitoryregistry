﻿using DormitoryRegistry.Controllers;
using DormitoryRegistry.View.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DormitoryRegistry.View
{
    public partial class MenuForm : Form
    {
        

        private ContentType _selected;

        public ContentType Selected
        {
            get { return _selected; }
            set
            {
                _selected = value;
                OnSelectionChanged();
            }
        }

        public event EventHandler SelectionChanged;

        public event EventHandler CloseRequired
        {
            add { bn_Exit.Click += value; }
            remove { bn_Exit.Click -= value; }
        }

        public bool LeadersVisible
        {
            get { return cb_Leaders.Visible; }
            set { cb_Leaders.Visible = value; }
        }

        public bool TeachersVisible
        {
            get { return cb_Teachers.Visible; }
            set { cb_Teachers.Visible = value; }
        }


        private List<CheckBox> _selectors;

        public MenuForm()
        {
            InitializeComponent();
            _selectors = new List<CheckBox>();

            cb_Leaders.Tag = ContentType.Leaders;
            cb_Occupation.Tag = ContentType.Occupation;
            cb_Settings.Tag = ContentType.Settings;
            cb_Students.Tag = ContentType.Students;
            cb_Teachers.Tag = ContentType.Teachers;

            _selectors.Add(cb_Leaders);
            _selectors.Add(cb_Occupation);
            _selectors.Add(cb_Settings);
            _selectors.Add(cb_Students);
            _selectors.Add(cb_Teachers);

            _selectors.ForEach(selector => 
            {
                selector.Click += Selector_Click;
            });
        }

        private void Selector_Click(object sender, EventArgs e)
        {
            if (sender is CheckBox)
            {
                Select(sender as CheckBox);
            }
        }

        private void Select(CheckBox selected)
        {
            _selectors.ForEach(selector =>
            {
                selector.Checked = selector == selected;

                if(selector.Checked)
                {
                    Selected = (ContentType)selector.Tag;
                }
            });
        }

        public void Reset()
        {
            Select(cb_Occupation);
        }

        private void OnSelectionChanged()
        {
            SelectionChanged?.Invoke(this, EventArgs.Empty);
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            Select(cb_Occupation);
        }
    }
}
