﻿namespace DormitoryRegistry.View
{
    partial class MenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cb_Occupation = new System.Windows.Forms.CheckBox();
            this.cb_Students = new System.Windows.Forms.CheckBox();
            this.cb_Teachers = new System.Windows.Forms.CheckBox();
            this.cb_Leaders = new System.Windows.Forms.CheckBox();
            this.cb_Settings = new System.Windows.Forms.CheckBox();
            this.bn_Exit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cb_Occupation
            // 
            this.cb_Occupation.Appearance = System.Windows.Forms.Appearance.Button;
            this.cb_Occupation.Checked = true;
            this.cb_Occupation.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cb_Occupation.Dock = System.Windows.Forms.DockStyle.Top;
            this.cb_Occupation.FlatAppearance.BorderSize = 0;
            this.cb_Occupation.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cb_Occupation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_Occupation.Font = new System.Drawing.Font("Segoe UI Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cb_Occupation.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cb_Occupation.Location = new System.Drawing.Point(0, 0);
            this.cb_Occupation.Margin = new System.Windows.Forms.Padding(0);
            this.cb_Occupation.Name = "cb_Occupation";
            this.cb_Occupation.Padding = new System.Windows.Forms.Padding(5);
            this.cb_Occupation.Size = new System.Drawing.Size(351, 55);
            this.cb_Occupation.TabIndex = 0;
            this.cb_Occupation.Text = "Foglalkozások";
            this.cb_Occupation.UseVisualStyleBackColor = true;
            // 
            // cb_Students
            // 
            this.cb_Students.Appearance = System.Windows.Forms.Appearance.Button;
            this.cb_Students.Dock = System.Windows.Forms.DockStyle.Top;
            this.cb_Students.FlatAppearance.BorderSize = 0;
            this.cb_Students.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cb_Students.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_Students.Font = new System.Drawing.Font("Segoe UI Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cb_Students.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cb_Students.Location = new System.Drawing.Point(0, 55);
            this.cb_Students.Margin = new System.Windows.Forms.Padding(0);
            this.cb_Students.Name = "cb_Students";
            this.cb_Students.Padding = new System.Windows.Forms.Padding(5);
            this.cb_Students.Size = new System.Drawing.Size(351, 55);
            this.cb_Students.TabIndex = 1;
            this.cb_Students.Text = "Tanulók";
            this.cb_Students.UseVisualStyleBackColor = true;
            // 
            // cb_Teachers
            // 
            this.cb_Teachers.Appearance = System.Windows.Forms.Appearance.Button;
            this.cb_Teachers.Dock = System.Windows.Forms.DockStyle.Top;
            this.cb_Teachers.FlatAppearance.BorderSize = 0;
            this.cb_Teachers.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cb_Teachers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_Teachers.Font = new System.Drawing.Font("Segoe UI Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cb_Teachers.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cb_Teachers.Location = new System.Drawing.Point(0, 110);
            this.cb_Teachers.Margin = new System.Windows.Forms.Padding(0);
            this.cb_Teachers.Name = "cb_Teachers";
            this.cb_Teachers.Padding = new System.Windows.Forms.Padding(5);
            this.cb_Teachers.Size = new System.Drawing.Size(351, 55);
            this.cb_Teachers.TabIndex = 2;
            this.cb_Teachers.Text = "Tanárok";
            this.cb_Teachers.UseVisualStyleBackColor = true;
            // 
            // cb_Leaders
            // 
            this.cb_Leaders.Appearance = System.Windows.Forms.Appearance.Button;
            this.cb_Leaders.Dock = System.Windows.Forms.DockStyle.Top;
            this.cb_Leaders.FlatAppearance.BorderSize = 0;
            this.cb_Leaders.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cb_Leaders.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_Leaders.Font = new System.Drawing.Font("Segoe UI Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cb_Leaders.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cb_Leaders.Location = new System.Drawing.Point(0, 165);
            this.cb_Leaders.Margin = new System.Windows.Forms.Padding(0);
            this.cb_Leaders.Name = "cb_Leaders";
            this.cb_Leaders.Padding = new System.Windows.Forms.Padding(5);
            this.cb_Leaders.Size = new System.Drawing.Size(351, 55);
            this.cb_Leaders.TabIndex = 3;
            this.cb_Leaders.Text = "Vezetők";
            this.cb_Leaders.UseVisualStyleBackColor = true;
            // 
            // cb_Settings
            // 
            this.cb_Settings.Appearance = System.Windows.Forms.Appearance.Button;
            this.cb_Settings.Dock = System.Windows.Forms.DockStyle.Top;
            this.cb_Settings.FlatAppearance.BorderSize = 0;
            this.cb_Settings.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.cb_Settings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cb_Settings.Font = new System.Drawing.Font("Segoe UI Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cb_Settings.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cb_Settings.Location = new System.Drawing.Point(0, 220);
            this.cb_Settings.Margin = new System.Windows.Forms.Padding(0);
            this.cb_Settings.Name = "cb_Settings";
            this.cb_Settings.Padding = new System.Windows.Forms.Padding(5);
            this.cb_Settings.Size = new System.Drawing.Size(351, 55);
            this.cb_Settings.TabIndex = 4;
            this.cb_Settings.Text = "Beállításaim";
            this.cb_Settings.UseVisualStyleBackColor = true;
            // 
            // bn_Exit
            // 
            this.bn_Exit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bn_Exit.FlatAppearance.BorderSize = 0;
            this.bn_Exit.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bn_Exit.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.bn_Exit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bn_Exit.Font = new System.Drawing.Font("Segoe UI Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bn_Exit.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.bn_Exit.Location = new System.Drawing.Point(0, 566);
            this.bn_Exit.Name = "bn_Exit";
            this.bn_Exit.Size = new System.Drawing.Size(351, 55);
            this.bn_Exit.TabIndex = 5;
            this.bn_Exit.Text = "Kilépés";
            this.bn_Exit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bn_Exit.UseVisualStyleBackColor = true;
            // 
            // MenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(351, 621);
            this.Controls.Add(this.bn_Exit);
            this.Controls.Add(this.cb_Settings);
            this.Controls.Add(this.cb_Leaders);
            this.Controls.Add(this.cb_Teachers);
            this.Controls.Add(this.cb_Students);
            this.Controls.Add(this.cb_Occupation);
            this.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MinimumSize = new System.Drawing.Size(100, 200);
            this.Name = "MenuForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "MenuForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox cb_Occupation;
        private System.Windows.Forms.CheckBox cb_Students;
        private System.Windows.Forms.CheckBox cb_Teachers;
        private System.Windows.Forms.CheckBox cb_Leaders;
        private System.Windows.Forms.CheckBox cb_Settings;
        private System.Windows.Forms.Button bn_Exit;
    }
}