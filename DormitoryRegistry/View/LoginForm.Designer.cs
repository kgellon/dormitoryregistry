﻿namespace DormitoryRegistry.View
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pn_Header = new System.Windows.Forms.Panel();
            this.lb_Title = new System.Windows.Forms.Label();
            this.bn_Close = new System.Windows.Forms.Button();
            this.lb_UserName = new System.Windows.Forms.Label();
            this.tb_Username = new System.Windows.Forms.TextBox();
            this.tlp_BaseLayout = new System.Windows.Forms.TableLayoutPanel();
            this.lb_Message = new System.Windows.Forms.Label();
            this.pn_Line2 = new System.Windows.Forms.Panel();
            this.pn_Line1 = new System.Windows.Forms.Panel();
            this.tb_Password = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pn_Header.SuspendLayout();
            this.tlp_BaseLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // pn_Header
            // 
            this.pn_Header.Controls.Add(this.lb_Title);
            this.pn_Header.Controls.Add(this.bn_Close);
            this.pn_Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.pn_Header.Location = new System.Drawing.Point(0, 0);
            this.pn_Header.Margin = new System.Windows.Forms.Padding(0);
            this.pn_Header.Name = "pn_Header";
            this.pn_Header.Size = new System.Drawing.Size(481, 35);
            this.pn_Header.TabIndex = 9;
            // 
            // lb_Title
            // 
            this.lb_Title.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_Title.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lb_Title.Font = new System.Drawing.Font("Segoe UI Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_Title.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lb_Title.Location = new System.Drawing.Point(0, 0);
            this.lb_Title.Name = "lb_Title";
            this.lb_Title.Size = new System.Drawing.Size(437, 35);
            this.lb_Title.TabIndex = 1;
            this.lb_Title.Text = "Belépés";
            this.lb_Title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // bn_Close
            // 
            this.bn_Close.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bn_Close.Dock = System.Windows.Forms.DockStyle.Right;
            this.bn_Close.FlatAppearance.BorderSize = 0;
            this.bn_Close.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlDark;
            this.bn_Close.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.bn_Close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bn_Close.Font = new System.Drawing.Font("Segoe UI Light", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bn_Close.ForeColor = System.Drawing.SystemColors.Control;
            this.bn_Close.Location = new System.Drawing.Point(437, 0);
            this.bn_Close.Name = "bn_Close";
            this.bn_Close.Size = new System.Drawing.Size(44, 35);
            this.bn_Close.TabIndex = 0;
            this.bn_Close.Text = "X";
            this.bn_Close.UseVisualStyleBackColor = false;
            this.bn_Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // lb_UserName
            // 
            this.lb_UserName.BackColor = System.Drawing.Color.Transparent;
            this.lb_UserName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lb_UserName.Font = new System.Drawing.Font("Segoe UI Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_UserName.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_UserName.Location = new System.Drawing.Point(51, 47);
            this.lb_UserName.Margin = new System.Windows.Forms.Padding(3);
            this.lb_UserName.Name = "lb_UserName";
            this.lb_UserName.Size = new System.Drawing.Size(378, 34);
            this.lb_UserName.TabIndex = 2;
            this.lb_UserName.Text = "Felhasználó név";
            this.lb_UserName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tb_Username
            // 
            this.tb_Username.BackColor = System.Drawing.SystemColors.Control;
            this.tb_Username.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Username.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_Username.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tb_Username.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tb_Username.Location = new System.Drawing.Point(58, 87);
            this.tb_Username.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.tb_Username.MaxLength = 50;
            this.tb_Username.Name = "tb_Username";
            this.tb_Username.Size = new System.Drawing.Size(371, 29);
            this.tb_Username.TabIndex = 0;
            this.tb_Username.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Username_KeyDown);
            // 
            // tlp_BaseLayout
            // 
            this.tlp_BaseLayout.BackColor = System.Drawing.Color.Transparent;
            this.tlp_BaseLayout.ColumnCount = 3;
            this.tlp_BaseLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlp_BaseLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tlp_BaseLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tlp_BaseLayout.Controls.Add(this.lb_Message, 1, 9);
            this.tlp_BaseLayout.Controls.Add(this.pn_Line2, 1, 7);
            this.tlp_BaseLayout.Controls.Add(this.pn_Line1, 1, 3);
            this.tlp_BaseLayout.Controls.Add(this.tb_Password, 1, 6);
            this.tlp_BaseLayout.Controls.Add(this.label1, 1, 5);
            this.tlp_BaseLayout.Controls.Add(this.lb_UserName, 1, 1);
            this.tlp_BaseLayout.Controls.Add(this.tb_Username, 1, 2);
            this.tlp_BaseLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp_BaseLayout.Location = new System.Drawing.Point(0, 35);
            this.tlp_BaseLayout.Margin = new System.Windows.Forms.Padding(0);
            this.tlp_BaseLayout.Name = "tlp_BaseLayout";
            this.tlp_BaseLayout.RowCount = 10;
            this.tlp_BaseLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp_BaseLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlp_BaseLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tlp_BaseLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.tlp_BaseLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlp_BaseLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlp_BaseLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tlp_BaseLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            this.tlp_BaseLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp_BaseLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlp_BaseLayout.Size = new System.Drawing.Size(481, 283);
            this.tlp_BaseLayout.TabIndex = 12;
            // 
            // lb_Message
            // 
            this.lb_Message.BackColor = System.Drawing.Color.Transparent;
            this.lb_Message.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lb_Message.Font = new System.Drawing.Font("Segoe UI Light", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_Message.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_Message.Location = new System.Drawing.Point(48, 262);
            this.lb_Message.Margin = new System.Windows.Forms.Padding(0);
            this.lb_Message.Name = "lb_Message";
            this.lb_Message.Size = new System.Drawing.Size(384, 21);
            this.lb_Message.TabIndex = 15;
            this.lb_Message.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pn_Line2
            // 
            this.pn_Line2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pn_Line2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_Line2.Location = new System.Drawing.Point(58, 216);
            this.pn_Line2.Margin = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.pn_Line2.Name = "pn_Line2";
            this.pn_Line2.Size = new System.Drawing.Size(374, 2);
            this.pn_Line2.TabIndex = 14;
            // 
            // pn_Line1
            // 
            this.pn_Line1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pn_Line1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_Line1.Location = new System.Drawing.Point(58, 119);
            this.pn_Line1.Margin = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.pn_Line1.Name = "pn_Line1";
            this.pn_Line1.Size = new System.Drawing.Size(374, 2);
            this.pn_Line1.TabIndex = 12;
            // 
            // tb_Password
            // 
            this.tb_Password.BackColor = System.Drawing.SystemColors.Control;
            this.tb_Password.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Password.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_Password.Font = new System.Drawing.Font("Segoe UI Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tb_Password.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tb_Password.Location = new System.Drawing.Point(58, 184);
            this.tb_Password.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.tb_Password.MaxLength = 50;
            this.tb_Password.Name = "tb_Password";
            this.tb_Password.Size = new System.Drawing.Size(371, 32);
            this.tb_Password.TabIndex = 1;
            this.tb_Password.UseSystemPasswordChar = true;
            this.tb_Password.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Password_KeyDown);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Segoe UI Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Location = new System.Drawing.Point(51, 144);
            this.label1.Margin = new System.Windows.Forms.Padding(3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(378, 34);
            this.label1.TabIndex = 13;
            this.label1.Text = "Jelszó";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 318);
            this.Controls.Add(this.tlp_BaseLayout);
            this.Controls.Add(this.pn_Header);
            this.Font = new System.Drawing.Font("Segoe UI Light", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "LoginForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "LoginForm";
            this.pn_Header.ResumeLayout(false);
            this.tlp_BaseLayout.ResumeLayout(false);
            this.tlp_BaseLayout.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pn_Header;
        private System.Windows.Forms.Label lb_Title;
        private System.Windows.Forms.Button bn_Close;
        private System.Windows.Forms.Label lb_UserName;
        private System.Windows.Forms.TextBox tb_Username;
        private System.Windows.Forms.TableLayoutPanel tlp_BaseLayout;
        private System.Windows.Forms.TextBox tb_Password;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lb_Message;
        private System.Windows.Forms.Panel pn_Line2;
        private System.Windows.Forms.Panel pn_Line1;
    }
}