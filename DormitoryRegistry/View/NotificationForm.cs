﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DormitoryRegistry.View
{
    public partial class NotificationForm : Form
    {
        public enum NotificationTypes
        {
            Information,
            Progress, 
            Error
        }

        public string Title
        {
            get { return lb_Title.Text; }
            set { lb_Title.Text = value; }
        }

        public string Message
        {
            get { return lb_Message.Text; }
            set
            {
                lb_Message.Text = value;
                Height = lb_Message.Text.Length > 100 ? 200 : 100;
                lb_Message.Font = new Font(lb_Message.Font.FontFamily, lb_Message.Text.Length > 100 ? 10F : 14F, lb_Message.Font.Style);
            }
        }

        public bool IsShown { get; private set; }

        public int Progress
        {
            get { return pb_Progress.Value; }
            set { pb_Progress.Value = Math.Max(0, Math.Min(100, value)); }
        }

        public bool ShowProgress
        {
            get { return pb_Progress.Visible; }
            set { pb_Progress.Visible = value; }
        }

        public bool CloseVisible
        {
            get { return bn_Close.Visible; }
            set { bn_Close.Visible = value; }
        }

        private NotificationTypes _notificationType;
        public NotificationTypes NotificationType
        {
            get { return _notificationType; }
            set
            {
                _notificationType = value;
                UpdateStyle();
            }
        }

        public NotificationForm()
        {
            InitializeComponent();
        }

        private void UpdateStyle()
        {
            switch(NotificationType)
            {
                case NotificationTypes.Information:
                    lb_Title.BackColor = SystemColors.ControlDarkDark;
                    bn_Close.BackColor = SystemColors.ControlDarkDark;
                    pb_Progress.Visible = false;
                    break;
                case NotificationTypes.Progress:
                    lb_Title.BackColor = SystemColors.ControlDarkDark;
                    bn_Close.BackColor = SystemColors.ControlDarkDark;
                    pb_Progress.Visible = true;
                    break;
                case NotificationTypes.Error:
                    lb_Title.BackColor = Color.DarkRed;
                    bn_Close.BackColor = Color.DarkRed;
                    pb_Progress.Visible = false;
                    break;
            }
        }

        private void Close_Click(object sender, EventArgs e)
        {
            Close();
        }

        protected override void OnShown(EventArgs e)
        {
            IsShown = true;
            base.OnShown(e);
        }

        protected override void OnClosed(EventArgs e)
        {
            IsShown = false;
            base.OnClosed(e);
        }
    }
}
