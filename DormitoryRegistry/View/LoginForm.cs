﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DormitoryRegistry.View
{
    public partial class LoginForm : Form
    {
        public const int MinimumLength = 5;

        public event EventHandler DataCheckRequired;

        public string Username
        {
            get { return tb_Username.Text; }
        }

        public string Password
        {
            get { return tb_Password.Text; }
        }

        public bool IsShown { get; private set; }

        public LoginForm()
        {
            InitializeComponent();
        }

        public void ShowInformationMessage(string message, bool enable)
        {
            lb_Message.ForeColor = SystemColors.ControlDarkDark;
            lb_Message.Text = message;
            lb_Message.Visible = true;
            tb_Username.Enabled = enable;
            tb_Password.Enabled = enable;
        }

        public void ShowErrorMessage(string message, bool enable)
        {
            lb_Message.ForeColor = Color.Maroon;
            lb_Message.Text = message;
            lb_Message.Visible = true;
            tb_Username.Enabled = enable;
            tb_Password.Enabled = enable;
        }

        public void HideMessage()
        {
            lb_Message.Visible = false;
        }

        public void Clear()
        {
            tb_Username.Text = string.Empty;
            tb_Password.Text = string.Empty;
            HideMessage();

            tb_Username.Select();
        }

        private void Close_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Username_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                if (Password.Length <= 0)
                {
                    tb_Password.Select();
                }
                else
                {
                    OnDataCheckRequired();
                }
            }
            else
            {
                HideMessage();
            }
        }

        private void Password_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                OnDataCheckRequired();
            }
            else
            {
                HideMessage();
            }
        }

        private void OnDataCheckRequired()
        {
            if (Username.Length < MinimumLength)
            {
                ShowInformationMessage(string.Format("Felhasználónévnek legalább {0} karakter hosszúnak kell lennie.", MinimumLength), true);
            }
            else if (Username.Length < MinimumLength)
            {
                ShowInformationMessage(string.Format("Jelszónak legalább {0} karakter hosszúnak kell lennie.", MinimumLength), true);
            }
            else
            {
                DataCheckRequired?.Invoke(this, EventArgs.Empty);
            }
        }

        protected override void OnShown(EventArgs e)
        {
            IsShown = true;
            base.OnShown(e);
        }

        protected override void OnClosed(EventArgs e)
        {
            IsShown = false;
            base.OnClosed(e);
        }

    }
}
