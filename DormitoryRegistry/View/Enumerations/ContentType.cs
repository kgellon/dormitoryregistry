﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DormitoryRegistry.View.Enumerations
{
    public enum ContentType
    {
        Occupation,
        Leaders,
        Teachers,
        Students,
        Settings
    }
}
