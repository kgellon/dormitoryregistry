﻿namespace DormitoryRegistry.View
{
    partial class NotificationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pb_Progress = new System.Windows.Forms.ProgressBar();
            this.pn_Header = new System.Windows.Forms.Panel();
            this.lb_Title = new System.Windows.Forms.Label();
            this.bn_Close = new System.Windows.Forms.Button();
            this.lb_Message = new System.Windows.Forms.Label();
            this.pn_Header.SuspendLayout();
            this.SuspendLayout();
            // 
            // pb_Progress
            // 
            this.pb_Progress.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.pb_Progress.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pb_Progress.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.pb_Progress.Location = new System.Drawing.Point(0, 90);
            this.pb_Progress.Margin = new System.Windows.Forms.Padding(0, 0, 1, 0);
            this.pb_Progress.Name = "pb_Progress";
            this.pb_Progress.Size = new System.Drawing.Size(500, 10);
            this.pb_Progress.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pb_Progress.TabIndex = 4;
            this.pb_Progress.Value = 100;
            // 
            // pn_Header
            // 
            this.pn_Header.Controls.Add(this.lb_Title);
            this.pn_Header.Controls.Add(this.bn_Close);
            this.pn_Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.pn_Header.Location = new System.Drawing.Point(0, 0);
            this.pn_Header.Margin = new System.Windows.Forms.Padding(0);
            this.pn_Header.Name = "pn_Header";
            this.pn_Header.Size = new System.Drawing.Size(500, 35);
            this.pn_Header.TabIndex = 6;
            // 
            // lb_Title
            // 
            this.lb_Title.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_Title.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lb_Title.Font = new System.Drawing.Font("Segoe UI Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_Title.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lb_Title.Location = new System.Drawing.Point(0, 0);
            this.lb_Title.Name = "lb_Title";
            this.lb_Title.Size = new System.Drawing.Size(456, 35);
            this.lb_Title.TabIndex = 1;
            this.lb_Title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // bn_Close
            // 
            this.bn_Close.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bn_Close.Dock = System.Windows.Forms.DockStyle.Right;
            this.bn_Close.FlatAppearance.BorderSize = 0;
            this.bn_Close.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlDark;
            this.bn_Close.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.bn_Close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bn_Close.Font = new System.Drawing.Font("Segoe UI Light", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bn_Close.ForeColor = System.Drawing.SystemColors.Control;
            this.bn_Close.Location = new System.Drawing.Point(456, 0);
            this.bn_Close.Name = "bn_Close";
            this.bn_Close.Size = new System.Drawing.Size(44, 35);
            this.bn_Close.TabIndex = 0;
            this.bn_Close.Text = "X";
            this.bn_Close.UseVisualStyleBackColor = false;
            this.bn_Close.Visible = false;
            this.bn_Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // lb_Message
            // 
            this.lb_Message.BackColor = System.Drawing.Color.Transparent;
            this.lb_Message.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lb_Message.Font = new System.Drawing.Font("Segoe UI Light", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_Message.Location = new System.Drawing.Point(0, 35);
            this.lb_Message.Name = "lb_Message";
            this.lb_Message.Padding = new System.Windows.Forms.Padding(20, 0, 0, 0);
            this.lb_Message.Size = new System.Drawing.Size(500, 55);
            this.lb_Message.TabIndex = 7;
            this.lb_Message.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // NotificationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 28F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(500, 100);
            this.Controls.Add(this.lb_Message);
            this.Controls.Add(this.pn_Header);
            this.Controls.Add(this.pb_Progress);
            this.Font = new System.Drawing.Font("Segoe UI Light", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(5, 6, 5, 6);
            this.MinimumSize = new System.Drawing.Size(500, 100);
            this.Name = "NotificationForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NotificationForm";
            this.Click += new System.EventHandler(this.Close_Click);
            this.pn_Header.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ProgressBar pb_Progress;
        private System.Windows.Forms.Panel pn_Header;
        private System.Windows.Forms.Label lb_Title;
        private System.Windows.Forms.Button bn_Close;
        private System.Windows.Forms.Label lb_Message;
    }
}