﻿using DormitoryRegistry.Controllers;
using DormitoryRegistry.View.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DormitoryRegistry.View
{
    public partial class MainForm : Form
    {
        public MenuForm MenuStrip;
        public ContentForm Content;

        private MainController _mainController;

        public event EventHandler SelectionChanged
        {
            add { MenuStrip.SelectionChanged += value; }
            remove { MenuStrip.SelectionChanged -= value; }
        }



        public string UserDisplayName
        {
            get { return lb_Name.Text; }
            set { lb_Name.Text = value; }
        }

        public MainForm(MainController mainController)
        {
            InitializeComponent();
            _mainController = mainController;
            InitializeInnerForms();
        }

        private void InitializeInnerForms()
        {
            MenuStrip = new MenuForm()
            {
                MdiParent = this,
                Dock = DockStyle.Fill
            };
            sc_BasicSplitter.Panel1.Controls.Add(MenuStrip);
            MenuStrip.CloseRequired += MenuForm_CloseRequired;

            Content = new ContentForm(_mainController)
            {
                MdiParent = this,
                Dock = DockStyle.Fill
            };
            sc_BasicSplitter.Panel2.Controls.Add(Content);
        }

        private void MenuForm_CloseRequired(object sender, EventArgs e)
        {
            _mainController.Close();
        }

        protected override void OnShown(EventArgs e)
        {
            MenuStrip.Show();
            Content.Show();
            base.OnShown(e);
        }
    }

}
