﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DormitoryRegistry.View.Enumerations;

namespace DormitoryRegistry.View.ContentPanels
{
    public partial class ContentPanelBase : UserControl
    {
        public string Title
        {
            get { return lb_Title.Text; }
            set { lb_Title.Text = value; }
        }

        public ContentPanelBase()
        {
            InitializeComponent();
        }
    }
}
