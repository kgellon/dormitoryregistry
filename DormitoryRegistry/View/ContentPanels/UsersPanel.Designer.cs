﻿namespace DormitoryRegistry.View.ContentPanels
{
    partial class UsersPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pn_NewUser = new System.Windows.Forms.Panel();
            this.tlp_NewUserBaseLayout = new System.Windows.Forms.TableLayoutPanel();
            this.pn_DataPanel1 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.tb_FirstName = new System.Windows.Forms.TextBox();
            this.lb_FirstName = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.tb_LastName = new System.Windows.Forms.TextBox();
            this.lb_LastName = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tb_Username = new System.Windows.Forms.TextBox();
            this.lb_UserName = new System.Windows.Forms.Label();
            this.pn_DataPanel2 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.tb_Password2 = new System.Windows.Forms.TextBox();
            this.lb_Password2 = new System.Windows.Forms.Label();
            this.pn_Password1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tb_Password1 = new System.Windows.Forms.TextBox();
            this.lb_Password1 = new System.Windows.Forms.Label();
            this.bn_SaveNewUser = new System.Windows.Forms.Button();
            this.lb_Error = new System.Windows.Forms.Label();
            this.lb_AddUserTitle = new System.Windows.Forms.Label();
            this.pn_UsersBorder = new System.Windows.Forms.Panel();
            this.dgv_Users = new System.Windows.Forms.DataGridView();
            this.LoginName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Role = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RoleId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UserId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Deleted = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Password = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pn_NewUser.SuspendLayout();
            this.tlp_NewUserBaseLayout.SuspendLayout();
            this.pn_DataPanel1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pn_DataPanel2.SuspendLayout();
            this.panel9.SuspendLayout();
            this.pn_Password1.SuspendLayout();
            this.pn_UsersBorder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Users)).BeginInit();
            this.SuspendLayout();
            // 
            // pn_NewUser
            // 
            this.pn_NewUser.Controls.Add(this.tlp_NewUserBaseLayout);
            this.pn_NewUser.Controls.Add(this.lb_AddUserTitle);
            this.pn_NewUser.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pn_NewUser.Location = new System.Drawing.Point(0, 308);
            this.pn_NewUser.Margin = new System.Windows.Forms.Padding(0);
            this.pn_NewUser.Name = "pn_NewUser";
            this.pn_NewUser.Size = new System.Drawing.Size(701, 292);
            this.pn_NewUser.TabIndex = 5;
            // 
            // tlp_NewUserBaseLayout
            // 
            this.tlp_NewUserBaseLayout.ColumnCount = 6;
            this.tlp_NewUserBaseLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp_NewUserBaseLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tlp_NewUserBaseLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlp_NewUserBaseLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tlp_NewUserBaseLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp_NewUserBaseLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 151F));
            this.tlp_NewUserBaseLayout.Controls.Add(this.pn_DataPanel1, 1, 1);
            this.tlp_NewUserBaseLayout.Controls.Add(this.pn_DataPanel2, 3, 1);
            this.tlp_NewUserBaseLayout.Controls.Add(this.bn_SaveNewUser, 5, 1);
            this.tlp_NewUserBaseLayout.Controls.Add(this.lb_Error, 1, 2);
            this.tlp_NewUserBaseLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp_NewUserBaseLayout.Location = new System.Drawing.Point(0, 35);
            this.tlp_NewUserBaseLayout.Margin = new System.Windows.Forms.Padding(0);
            this.tlp_NewUserBaseLayout.Name = "tlp_NewUserBaseLayout";
            this.tlp_NewUserBaseLayout.RowCount = 3;
            this.tlp_NewUserBaseLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlp_NewUserBaseLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlp_NewUserBaseLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlp_NewUserBaseLayout.Size = new System.Drawing.Size(701, 257);
            this.tlp_NewUserBaseLayout.TabIndex = 18;
            // 
            // pn_DataPanel1
            // 
            this.pn_DataPanel1.Controls.Add(this.panel6);
            this.pn_DataPanel1.Controls.Add(this.panel2);
            this.pn_DataPanel1.Controls.Add(this.panel1);
            this.pn_DataPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_DataPanel1.Location = new System.Drawing.Point(65, 20);
            this.pn_DataPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.pn_DataPanel1.Name = "pn_DataPanel1";
            this.pn_DataPanel1.Size = new System.Drawing.Size(200, 217);
            this.pn_DataPanel1.TabIndex = 0;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Controls.Add(this.tb_FirstName);
            this.panel6.Controls.Add(this.lb_FirstName);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 148);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(200, 74);
            this.panel6.TabIndex = 18;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 63);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(200, 2);
            this.panel7.TabIndex = 4;
            // 
            // tb_FirstName
            // 
            this.tb_FirstName.BackColor = System.Drawing.SystemColors.Control;
            this.tb_FirstName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_FirstName.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_FirstName.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tb_FirstName.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tb_FirstName.Location = new System.Drawing.Point(0, 34);
            this.tb_FirstName.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.tb_FirstName.MaxLength = 50;
            this.tb_FirstName.Name = "tb_FirstName";
            this.tb_FirstName.Size = new System.Drawing.Size(200, 29);
            this.tb_FirstName.TabIndex = 3;
            // 
            // lb_FirstName
            // 
            this.lb_FirstName.BackColor = System.Drawing.Color.Transparent;
            this.lb_FirstName.Dock = System.Windows.Forms.DockStyle.Top;
            this.lb_FirstName.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_FirstName.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_FirstName.Location = new System.Drawing.Point(0, 0);
            this.lb_FirstName.Margin = new System.Windows.Forms.Padding(3);
            this.lb_FirstName.Name = "lb_FirstName";
            this.lb_FirstName.Size = new System.Drawing.Size(200, 34);
            this.lb_FirstName.TabIndex = 2;
            this.lb_FirstName.Text = "Keresztnév";
            this.lb_FirstName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.tb_LastName);
            this.panel2.Controls.Add(this.lb_LastName);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 74);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 74);
            this.panel2.TabIndex = 17;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 63);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(200, 2);
            this.panel5.TabIndex = 4;
            // 
            // tb_LastName
            // 
            this.tb_LastName.BackColor = System.Drawing.SystemColors.Control;
            this.tb_LastName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_LastName.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_LastName.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tb_LastName.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tb_LastName.Location = new System.Drawing.Point(0, 34);
            this.tb_LastName.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.tb_LastName.MaxLength = 50;
            this.tb_LastName.Name = "tb_LastName";
            this.tb_LastName.Size = new System.Drawing.Size(200, 29);
            this.tb_LastName.TabIndex = 3;
            // 
            // lb_LastName
            // 
            this.lb_LastName.BackColor = System.Drawing.Color.Transparent;
            this.lb_LastName.Dock = System.Windows.Forms.DockStyle.Top;
            this.lb_LastName.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_LastName.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_LastName.Location = new System.Drawing.Point(0, 0);
            this.lb_LastName.Margin = new System.Windows.Forms.Padding(3);
            this.lb_LastName.Name = "lb_LastName";
            this.lb_LastName.Size = new System.Drawing.Size(200, 34);
            this.lb_LastName.TabIndex = 2;
            this.lb_LastName.Text = "Vezetéknév";
            this.lb_LastName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.tb_Username);
            this.panel1.Controls.Add(this.lb_UserName);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 74);
            this.panel1.TabIndex = 16;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 63);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(200, 2);
            this.panel3.TabIndex = 4;
            // 
            // tb_Username
            // 
            this.tb_Username.BackColor = System.Drawing.SystemColors.Control;
            this.tb_Username.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Username.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_Username.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tb_Username.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tb_Username.Location = new System.Drawing.Point(0, 34);
            this.tb_Username.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.tb_Username.MaxLength = 50;
            this.tb_Username.Name = "tb_Username";
            this.tb_Username.Size = new System.Drawing.Size(200, 29);
            this.tb_Username.TabIndex = 3;
            this.tb_Username.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Username_KeyDown);
            // 
            // lb_UserName
            // 
            this.lb_UserName.BackColor = System.Drawing.Color.Transparent;
            this.lb_UserName.Dock = System.Windows.Forms.DockStyle.Top;
            this.lb_UserName.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_UserName.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_UserName.Location = new System.Drawing.Point(0, 0);
            this.lb_UserName.Margin = new System.Windows.Forms.Padding(3);
            this.lb_UserName.Name = "lb_UserName";
            this.lb_UserName.Size = new System.Drawing.Size(200, 34);
            this.lb_UserName.TabIndex = 2;
            this.lb_UserName.Text = "Felhasználónév";
            this.lb_UserName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pn_DataPanel2
            // 
            this.pn_DataPanel2.Controls.Add(this.panel9);
            this.pn_DataPanel2.Controls.Add(this.pn_Password1);
            this.pn_DataPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_DataPanel2.Location = new System.Drawing.Point(285, 20);
            this.pn_DataPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.pn_DataPanel2.Name = "pn_DataPanel2";
            this.pn_DataPanel2.Size = new System.Drawing.Size(200, 217);
            this.pn_DataPanel2.TabIndex = 1;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.panel10);
            this.panel9.Controls.Add(this.tb_Password2);
            this.panel9.Controls.Add(this.lb_Password2);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(0, 71);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(200, 71);
            this.panel9.TabIndex = 20;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 66);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(200, 2);
            this.panel10.TabIndex = 16;
            // 
            // tb_Password2
            // 
            this.tb_Password2.BackColor = System.Drawing.SystemColors.Control;
            this.tb_Password2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Password2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_Password2.Font = new System.Drawing.Font("Segoe UI Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tb_Password2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tb_Password2.Location = new System.Drawing.Point(0, 34);
            this.tb_Password2.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.tb_Password2.MaxLength = 50;
            this.tb_Password2.Name = "tb_Password2";
            this.tb_Password2.Size = new System.Drawing.Size(200, 32);
            this.tb_Password2.TabIndex = 15;
            this.tb_Password2.UseSystemPasswordChar = true;
            this.tb_Password2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Password_KeyDown);
            // 
            // lb_Password2
            // 
            this.lb_Password2.BackColor = System.Drawing.Color.Transparent;
            this.lb_Password2.Dock = System.Windows.Forms.DockStyle.Top;
            this.lb_Password2.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_Password2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_Password2.Location = new System.Drawing.Point(0, 0);
            this.lb_Password2.Margin = new System.Windows.Forms.Padding(3);
            this.lb_Password2.Name = "lb_Password2";
            this.lb_Password2.Size = new System.Drawing.Size(200, 34);
            this.lb_Password2.TabIndex = 14;
            this.lb_Password2.Text = "Jelszó ismételve";
            this.lb_Password2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pn_Password1
            // 
            this.pn_Password1.Controls.Add(this.panel4);
            this.pn_Password1.Controls.Add(this.tb_Password1);
            this.pn_Password1.Controls.Add(this.lb_Password1);
            this.pn_Password1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pn_Password1.Location = new System.Drawing.Point(0, 0);
            this.pn_Password1.Name = "pn_Password1";
            this.pn_Password1.Size = new System.Drawing.Size(200, 71);
            this.pn_Password1.TabIndex = 19;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 66);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(200, 2);
            this.panel4.TabIndex = 16;
            // 
            // tb_Password1
            // 
            this.tb_Password1.BackColor = System.Drawing.SystemColors.Control;
            this.tb_Password1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_Password1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_Password1.Font = new System.Drawing.Font("Segoe UI Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tb_Password1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.tb_Password1.Location = new System.Drawing.Point(0, 34);
            this.tb_Password1.Margin = new System.Windows.Forms.Padding(10, 3, 3, 3);
            this.tb_Password1.MaxLength = 50;
            this.tb_Password1.Name = "tb_Password1";
            this.tb_Password1.Size = new System.Drawing.Size(200, 32);
            this.tb_Password1.TabIndex = 15;
            this.tb_Password1.UseSystemPasswordChar = true;
            this.tb_Password1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Password_KeyDown);
            // 
            // lb_Password1
            // 
            this.lb_Password1.BackColor = System.Drawing.Color.Transparent;
            this.lb_Password1.Dock = System.Windows.Forms.DockStyle.Top;
            this.lb_Password1.Font = new System.Drawing.Font("Segoe UI Light", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_Password1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_Password1.Location = new System.Drawing.Point(0, 0);
            this.lb_Password1.Margin = new System.Windows.Forms.Padding(3);
            this.lb_Password1.Name = "lb_Password1";
            this.lb_Password1.Size = new System.Drawing.Size(200, 34);
            this.lb_Password1.TabIndex = 14;
            this.lb_Password1.Text = "Jelszó";
            this.lb_Password1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // bn_SaveNewUser
            // 
            this.bn_SaveNewUser.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bn_SaveNewUser.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bn_SaveNewUser.FlatAppearance.BorderSize = 0;
            this.bn_SaveNewUser.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bn_SaveNewUser.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.bn_SaveNewUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bn_SaveNewUser.Font = new System.Drawing.Font("Segoe UI Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.bn_SaveNewUser.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.bn_SaveNewUser.Location = new System.Drawing.Point(553, 193);
            this.bn_SaveNewUser.Name = "bn_SaveNewUser";
            this.bn_SaveNewUser.Size = new System.Drawing.Size(145, 41);
            this.bn_SaveNewUser.TabIndex = 6;
            this.bn_SaveNewUser.Text = "Mentés";
            this.bn_SaveNewUser.UseVisualStyleBackColor = false;
            this.bn_SaveNewUser.Click += new System.EventHandler(this.SaveNewUser_Click);
            // 
            // lb_Error
            // 
            this.lb_Error.BackColor = System.Drawing.Color.Transparent;
            this.lb_Error.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lb_Error.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_Error.ForeColor = System.Drawing.Color.Maroon;
            this.lb_Error.Location = new System.Drawing.Point(68, 240);
            this.lb_Error.Margin = new System.Windows.Forms.Padding(3);
            this.lb_Error.Name = "lb_Error";
            this.lb_Error.Size = new System.Drawing.Size(194, 14);
            this.lb_Error.TabIndex = 7;
            this.lb_Error.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lb_AddUserTitle
            // 
            this.lb_AddUserTitle.BackColor = System.Drawing.Color.Transparent;
            this.lb_AddUserTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lb_AddUserTitle.Font = new System.Drawing.Font("Segoe UI Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_AddUserTitle.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lb_AddUserTitle.Location = new System.Drawing.Point(0, 0);
            this.lb_AddUserTitle.Name = "lb_AddUserTitle";
            this.lb_AddUserTitle.Size = new System.Drawing.Size(701, 35);
            this.lb_AddUserTitle.TabIndex = 3;
            this.lb_AddUserTitle.Text = "Hozzáadás";
            this.lb_AddUserTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pn_UsersBorder
            // 
            this.pn_UsersBorder.Controls.Add(this.dgv_Users);
            this.pn_UsersBorder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pn_UsersBorder.Location = new System.Drawing.Point(0, 58);
            this.pn_UsersBorder.Name = "pn_UsersBorder";
            this.pn_UsersBorder.Padding = new System.Windows.Forms.Padding(20);
            this.pn_UsersBorder.Size = new System.Drawing.Size(701, 250);
            this.pn_UsersBorder.TabIndex = 6;
            // 
            // dgv_Users
            // 
            this.dgv_Users.AllowUserToAddRows = false;
            this.dgv_Users.AllowUserToDeleteRows = false;
            this.dgv_Users.AllowUserToOrderColumns = true;
            this.dgv_Users.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.NullValue = "N/A";
            dataGridViewCellStyle1.Padding = new System.Windows.Forms.Padding(0, 2, 0, 2);
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_Users.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_Users.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_Users.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_Users.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv_Users.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Users.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_Users.ColumnHeadersHeight = 40;
            this.dgv_Users.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_Users.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LoginName,
            this.LastName,
            this.FirstName,
            this.Role,
            this.RoleId,
            this.UserId,
            this.Deleted,
            this.Password});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.Padding = new System.Windows.Forms.Padding(0, 2, 0, 2);
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_Users.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_Users.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_Users.EnableHeadersVisualStyles = false;
            this.dgv_Users.Location = new System.Drawing.Point(20, 20);
            this.dgv_Users.Name = "dgv_Users";
            this.dgv_Users.RowHeadersVisible = false;
            this.dgv_Users.Size = new System.Drawing.Size(661, 210);
            this.dgv_Users.TabIndex = 3;
            // 
            // LoginName
            // 
            this.LoginName.DataPropertyName = "LoginName";
            this.LoginName.HeaderText = "Felhasználónév";
            this.LoginName.MinimumWidth = 100;
            this.LoginName.Name = "LoginName";
            // 
            // LastName
            // 
            this.LastName.DataPropertyName = "LastName";
            this.LastName.HeaderText = "Vezetéknév";
            this.LastName.MinimumWidth = 100;
            this.LastName.Name = "LastName";
            // 
            // FirstName
            // 
            this.FirstName.DataPropertyName = "FirstName";
            this.FirstName.HeaderText = "Keresztnév";
            this.FirstName.MinimumWidth = 100;
            this.FirstName.Name = "FirstName";
            // 
            // Role
            // 
            this.Role.DataPropertyName = "Role";
            this.Role.HeaderText = "Role";
            this.Role.Name = "Role";
            this.Role.Visible = false;
            // 
            // RoleId
            // 
            this.RoleId.DataPropertyName = "RoleId";
            this.RoleId.HeaderText = "Szerepkör azonosító";
            this.RoleId.Name = "RoleId";
            this.RoleId.Visible = false;
            // 
            // UserId
            // 
            this.UserId.DataPropertyName = "UserId";
            this.UserId.HeaderText = "Azonosító";
            this.UserId.Name = "UserId";
            this.UserId.Visible = false;
            // 
            // Deleted
            // 
            this.Deleted.DataPropertyName = "Deleted";
            this.Deleted.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Deleted.HeaderText = "Törölt";
            this.Deleted.Name = "Deleted";
            this.Deleted.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Deleted.Visible = false;
            // 
            // Password
            // 
            this.Password.DataPropertyName = "Password";
            this.Password.HeaderText = "Jelszó";
            this.Password.Name = "Password";
            this.Password.Visible = false;
            // 
            // UsersPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.pn_UsersBorder);
            this.Controls.Add(this.pn_NewUser);
            this.Name = "UsersPanel";
            this.Size = new System.Drawing.Size(701, 600);
            this.Title = "Felhasználók";
            this.Controls.SetChildIndex(this.pn_NewUser, 0);
            this.Controls.SetChildIndex(this.pn_UsersBorder, 0);
            this.pn_NewUser.ResumeLayout(false);
            this.tlp_NewUserBaseLayout.ResumeLayout(false);
            this.pn_DataPanel1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pn_DataPanel2.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.pn_Password1.ResumeLayout(false);
            this.pn_Password1.PerformLayout();
            this.pn_UsersBorder.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Users)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pn_NewUser;
        private System.Windows.Forms.Panel pn_UsersBorder;
        private System.Windows.Forms.DataGridView dgv_Users;
        private System.Windows.Forms.Label lb_AddUserTitle;
        private System.Windows.Forms.Label lb_UserName;
        private System.Windows.Forms.TableLayoutPanel tlp_NewUserBaseLayout;
        private System.Windows.Forms.Panel pn_DataPanel1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox tb_FirstName;
        private System.Windows.Forms.Label lb_FirstName;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox tb_LastName;
        private System.Windows.Forms.Label lb_LastName;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox tb_Username;
        private System.Windows.Forms.Panel pn_DataPanel2;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.TextBox tb_Password2;
        private System.Windows.Forms.Label lb_Password2;
        private System.Windows.Forms.Panel pn_Password1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox tb_Password1;
        private System.Windows.Forms.Label lb_Password1;
        private System.Windows.Forms.Button bn_SaveNewUser;
        private System.Windows.Forms.Label lb_Error;
        private System.Windows.Forms.DataGridViewTextBoxColumn LoginName;
        private System.Windows.Forms.DataGridViewTextBoxColumn LastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn FirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Role;
        private System.Windows.Forms.DataGridViewTextBoxColumn RoleId;
        private System.Windows.Forms.DataGridViewTextBoxColumn UserId;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Deleted;
        private System.Windows.Forms.DataGridViewTextBoxColumn Password;
    }
}
