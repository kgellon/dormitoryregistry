﻿using System;
using System.Windows.Forms;
using DormitoryRegistry.View.Enumerations;
using System.Text.RegularExpressions;
using DormitoryRegistry.EventModels;
using DormitoryRegistry.Extensions;

namespace DormitoryRegistry.View.ContentPanels
{
    public partial class UsersPanel : ContentPanelBase
    {

        private const string UsernameFilterPattern = "^[a-zA-Z][a-zA-Z0-9]*$";

        public event NewUserEventHandler NewSaveRequired; 

        private ContentType _contentType;
        public ContentType ContentType
        {
            get { return _contentType; }
            set
            {
                _contentType = value;
                UpdatePanel();
            }
        }

        public bool AddEnabled
        {
            get { return pn_NewUser.Visible; }
            set { pn_NewUser.Visible = value; }
        }

        public DataGridView UserGridView
        {
            get { return dgv_Users; }
            set { dgv_Users = value; }
        }

        public UsersPanel()
        {
            InitializeComponent();
        }


        public void ShowErrorMessage(string message)
        {
            lb_Error.Text = message;
        }

        public void ClearMessage()
        {
            lb_Error.Text = string.Empty;
        }

        public void ClearFileds()
        {
            tb_FirstName.Text = string.Empty;
            tb_LastName.Text = string.Empty;
            tb_Username.Text = string.Empty;
            tb_Password1.Text = string.Empty;
            tb_Password2.Text = string.Empty;
        }

        private void SaveNewUser_Click(object sender, EventArgs e)
        {
            if(tb_Username.Text.Length < 5)
            {
                ShowErrorMessage("Felhasználónév túl rövid...");
                tb_Username.Select();
            }
            else if (Regex.IsMatch(tb_Username.Text, UsernameFilterPattern) == false)
            {
                ShowErrorMessage("Csak alfanumerikus karakterket lehetnek...");
                tb_Username.Select();
            }
            else if (tb_Password1.Text.Length < 6)
            {
                ShowErrorMessage("Jelszó túl rövid...");
                tb_Password1.Select();
            }
            else if (tb_Password1.Text != tb_Password2.Text)
            {
                ShowErrorMessage("A két jelszó nem azonos...");
                tb_Password2.Select();
            }
            else
            {
                OnNewSaveRequired();
            }
        }

        private void UpdatePanel()
        {
            switch (ContentType)
            {
                case ContentType.Leaders:
                    Title = "Vezetők";
                    lb_AddUserTitle.Text = "Új vezető hozzáadása";
                    break;
                case ContentType.Students:
                    Title = "Tanulók";
                    lb_AddUserTitle.Text = "Új tanuló hozzáadása";
                    break;
                case ContentType.Teachers:
                    Title = "Tanárok";
                    lb_AddUserTitle.Text = "Új tanár hozzáadása";
                    break;
            }
        }

        private void OnNewSaveRequired()
        {
            if(NewSaveRequired != null)
            {
                NewUserEventArgs newArgs = new NewUserEventArgs()
                {
                    FirstName = tb_FirstName.Text,
                    LastName = tb_LastName.Text,
                    LoginName = tb_Username.Text,
                    Password = tb_Password1.Text.ToSHA256String(),
                    ContentType = ContentType
                };

                NewSaveRequired?.Invoke(this, newArgs);
            }
        }

        private void Username_KeyDown(object sender, KeyEventArgs e)
        {

            if(e.KeyData == Keys.Enter)
            {
                tb_LastName.Select();
            }
            else
            {
                ClearMessage();
            }
        }

        private void Password_KeyDown(object sender, KeyEventArgs e)
        {
            
        }
    }
}
