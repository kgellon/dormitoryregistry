﻿namespace DormitoryRegistry.View
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pn_Header = new System.Windows.Forms.Panel();
            this.lb_Name = new System.Windows.Forms.Label();
            this.sc_BasicSplitter = new System.Windows.Forms.SplitContainer();
            this.pn_Header.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sc_BasicSplitter)).BeginInit();
            this.sc_BasicSplitter.SuspendLayout();
            this.SuspendLayout();
            // 
            // pn_Header
            // 
            this.pn_Header.Controls.Add(this.lb_Name);
            this.pn_Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.pn_Header.Location = new System.Drawing.Point(0, 0);
            this.pn_Header.Name = "pn_Header";
            this.pn_Header.Size = new System.Drawing.Size(1008, 41);
            this.pn_Header.TabIndex = 1;
            // 
            // lb_Name
            // 
            this.lb_Name.BackColor = System.Drawing.Color.Transparent;
            this.lb_Name.Dock = System.Windows.Forms.DockStyle.Right;
            this.lb_Name.Font = new System.Drawing.Font("Segoe UI Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lb_Name.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lb_Name.Location = new System.Drawing.Point(553, 0);
            this.lb_Name.Margin = new System.Windows.Forms.Padding(3);
            this.lb_Name.Name = "lb_Name";
            this.lb_Name.Size = new System.Drawing.Size(455, 41);
            this.lb_Name.TabIndex = 14;
            this.lb_Name.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // sc_BasicSplitter
            // 
            this.sc_BasicSplitter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sc_BasicSplitter.Location = new System.Drawing.Point(0, 41);
            this.sc_BasicSplitter.Name = "sc_BasicSplitter";
            this.sc_BasicSplitter.Panel1MinSize = 200;
            this.sc_BasicSplitter.Size = new System.Drawing.Size(1008, 520);
            this.sc_BasicSplitter.SplitterDistance = 335;
            this.sc_BasicSplitter.TabIndex = 2;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(1008, 561);
            this.Controls.Add(this.sc_BasicSplitter);
            this.Controls.Add(this.pn_Header);
            this.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.IsMdiContainer = true;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MinimumSize = new System.Drawing.Size(1024, 600);
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Dormitory Registry";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.pn_Header.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sc_BasicSplitter)).EndInit();
            this.sc_BasicSplitter.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pn_Header;
        private System.Windows.Forms.SplitContainer sc_BasicSplitter;
        private System.Windows.Forms.Label lb_Name;
    }
}

