﻿using DormitoryRegistry.View.Enumerations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DormitoryRegistry.EventModels
{
    public delegate void NewUserEventHandler(object sender, NewUserEventArgs e); 
    public class NewUserEventArgs: EventArgs
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string LoginName { get; set; }

        public string Password { get; set; }

        public ContentType ContentType { get; set; }

        public NewUserEventArgs()
        { }

    }
}
